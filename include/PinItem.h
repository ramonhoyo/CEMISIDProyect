#ifndef PINITEM_H
#define PINITEM_H

#include <QLine>
#include <QGraphicsLineItem>
#include <QGraphicsEllipseItem>
#include <QtCore/QObject>
#include <QGraphicsEllipseItem>

enum class PINTYPE{
  INPUT, OUTPUT
};

class PinItem : public QObject, public QGraphicsEllipseItem
{
  Q_OBJECT
public:
  PinItem(QGraphicsItem * parent = 0);
  PinItem(qreal _separation =0,PINTYPE _type = PINTYPE::INPUT , QGraphicsItem * parent = 0);
  PinItem(const QRectF & rect,PINTYPE _type = PINTYPE::INPUT , QGraphicsItem * parent = 0);
  PinItem(qreal x, qreal y, qreal width, qreal height,qreal _separation =0,
          PINTYPE _type = PINTYPE::INPUT ,QGraphicsItem * parent = 0);

inline qreal getSepartation(){ return this->separation;}

signals:
  void positionChanged(QPointF);

public slots:

private:
  qreal separation;
  PINTYPE type;
};

#endif // WIREITEM_H
