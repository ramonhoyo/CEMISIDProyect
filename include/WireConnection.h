#ifndef WIRECONNECTION_H
#define WIRECONNECTION_H

#include <QtCore/QObject>
#include <QGraphicsPathItem>
#include <QPainterPath>
class WireConnection : public QObject , public QGraphicsPathItem
{
  Q_OBJECT
public:
  explicit WireConnection(QGraphicsItem * parent = 0);
  WireConnection(QPointF _start,QPointF _end, QGraphicsItem * parent = 0);

  QPointF startPoint();
  QPointF endPoint();

signals:
  void startPointChanged(QPointF);
  void endPointChanged(QPointF);
  void changedPoints(QPointF,QPointF);

public slots:
  void adjust(const QPointF &start, const QPointF &end);
  void ajustStart(QPointF _start);
  void ajustEnd(QPointF);

protected:
  void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void CalculatePoints(const QPointF &start, const QPointF &end);

private:

  QVector<QPointF> breakPoints;
  QPainterPath path;
  int penWidth;
  QColor color;
  //  QString nameGateSender;
  //  QString nameGateRecepter;
};

#endif // WIRECONNECTION_H
