#ifndef GRAPHISCVIEW_H
#define GRAPHISCVIEW_H

#include <QGraphicsView>
#include <QtCore/QObject>
#include <QPair>

#include "GraphicsModuleItem.h"
#include "PinItem.h"
#include "WireConnection.h"
#include "GraphicsScene.h"
#include "Gate.h"
#include "Module.h"

//QPair<QGraphicsModuleItem*,QLine*> connectionsPinModule;
//QList<connectionsPinModule> connectionsList;

class GraphicsView : public QGraphicsView
{
  Q_OBJECT
public:
  GraphicsView(QWidget * parent = 0);
  GraphicsView(QGraphicsScene * scene, QWidget * parent = 0);
  ~GraphicsView();

  int setModuleDataBase(QHash<QString,Module*> *moduleDataBase);
  int addModuleToScene(Module* module);
  void zoomIn();
  void zoomOut();

signals:
  void moduleChangedPosition(QPoint);
  void moduleAddToProject(Module *);

protected:
  void dropEvent(QDropEvent * event) ;
  void dragEnterEvent(QDragEnterEvent *event) ;
  void dragMoveEvent(QDragMoveEvent *event) ;
  void mouseMoveEvent(QMouseEvent * event);
  void mouseDoubleClickEvent(QMouseEvent * event);
  void mouseReleaseEvent(QMouseEvent * event);
  void mousePressEvent(QMouseEvent * event);


private:
  QHash<QString,Module*>* m_modulesDataBase;
  QList<GraphicsModuleItem*> m_itemsCreated;
  QList<PinItem*> m_pins;
  QList<WireConnection*> m_pinsCreated;
  QPoint m_intialPointConection;
  bool m_moduleSelect;
  bool m_pinSelectect;
};

#endif // GRAPHISCVIEW_H
