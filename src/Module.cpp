#include "Module.h"

Module::Module()
{
  m_circuitType = ModuleType::MODULE_GATE;
  m_moduleName = "module";
  m_moduleImgDir = "NONE";
  m_positionModule = QPointF(0.0,0.0);
  m_width = 100;
  m_height = 100;
  m_numPinIn = 2;
  m_numPinOut = 1;
  m_dataFlowType = "bool";
}

Module::Module(const QString &name, const QString &imgDir, const ModuleType &type,
               const QList<Module *> &modulesList, const QList<ModuleSignalConnect> &signalsList,
               const QPointF &modulePos, const size_t &width, const size_t &height,
               const size_t &pinsIn, const size_t &pinsOut, const QString & dataFlow)
{
  m_circuitType = type;
  m_moduleName = name;
  m_moduleImgDir = imgDir;
  m_modulesInternalList = modulesList;
  m_moduleSignalInternalList = signalsList;
  m_positionModule = modulePos;
  m_width = width;
  m_height = height;
  m_numPinIn = pinsIn;
  m_numPinOut = pinsOut;
  m_dataFlowType = dataFlow;
}

Module::Module(Module &module)
{
  m_circuitType = module.circuitType();
  m_moduleName = module.name();
  m_moduleImgDir = module.imgDir();
  m_modulesInternalList = module.modulesInternalList();
  m_moduleSignalInternalList = module.moduleSignalInternalList();
  m_positionModule = module.position();
  m_width = module.width();
  m_height = module.height();
  m_numPinIn = module.numPinsIn();
  m_numPinOut = module.numPinsOut();
  m_dataFlowType = module.dataFlowType();
}

int Module::addPinIn(int num)
{
  if(num < 1)
    return 1;
  m_numPinIn+= num;
  return 0;
}

int Module::addPinIn(const QString &nameInternaModule, int num)
{
  if(num < 1)
    return 2;
  for(auto it : m_modulesInternalList)
  {
    if(it->name() == nameInternaModule)
    {
      it->addPinIn(num);
      return 0;
    }
  }
  return 2;
}

int Module::addPinOut(int num)
{
  if(num < 1)
    return 3;
  m_numPinOut += num;
  return 0;
}

int Module::addPinOut(const QString &nameInternaModule, int num)
{
  if(num < 1)
    return 4;
  for(auto it : m_modulesInternalList)
  {
    if(it->name() == nameInternaModule)
    {
      it->addPinOut(num);
      return 0;
    }
  }
  return 4;
}

int Module::setPinIn(int num)
{
  if(num < 1)
    return 5;
  m_numPinIn = num;
  return 0;
}

int Module::setPinIn(const QString &nameInternaModule, int num)
{
  if(num < 1)
    return 6;
  for(auto it : m_modulesInternalList)
  {
    if(it->name() == nameInternaModule)
    {
      it->setPinIn(num);
      return 0;
    }
  }
  return 6;
}

int Module::setPinOut(int num)
{
  if(num < 1)
    return 7;
  m_numPinOut = num;
  return 0;
}

int Module::setPinOut(const QString &nameInternaModule, int num)
{
  if(num < 1)
    return 8;
  for(auto it : m_modulesInternalList)
  {
    if(it->name() == nameInternaModule)
    {
      it->setPinOut(num);
      return 0;
    }
  }
  return 8;
}

int Module::addModule(Module *module)
{
  if(module == nullptr)
    return 9;
  m_modulesInternalList.append(module);
  return 0;
}

int Module::addSignalModuleInternal(const ModuleSignalConnect &signal)
{
  m_moduleSignalInternalList.append(signal);
  return 0;
}

int Module::addSignalModuleInternal(const QString &fst, const QString &snd, const QString &name)
{
  addSignalModuleInternal(std::make_tuple(fst ,snd, name));
  return 0;
}

int Module::setPosition(const QPointF &newPoint)
{
  if(newPoint.isNull())
    return 10;
  m_positionModule = newPoint;
  return 0;
}

int Module::setHeight(size_t height)
{
  if(height == 0)
    return 11;
  m_height = height;
  return 0;
}

int Module::setWidth(size_t width)
{
  if(width == 0)
    return 12;
  m_width = width;
  return 0;
}

