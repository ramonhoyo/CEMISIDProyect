#include "ModuleModel.h"
#include <QIcon>
#include <QMimeData>
#include <QDebug>
ModuleModel::ModuleModel(QObject *parent):
  QAbstractListModel(parent)
{

}

ModuleModel::ModuleModel(QList<Module*> &modules, QObject *parent):
  m_modules(modules), QAbstractListModel(parent)
{
}

int ModuleModel::rowCount(const QModelIndex &parent) const
{
  if (parent.isValid())
      return 0;
  else
      return m_modules.size();
}

QVariant ModuleModel::data(const QModelIndex &index, int role) const
{

  if (!index.isValid())
      return QVariant();

  if (role == Qt::DecorationRole)
  {
    if(m_modules.value(index.row())->imgDir() == "NONE")
    {

      int height = m_modules.value(index.row())->height();
      int width = m_modules.value(index.row())->width();
      QPixmap * module =  new QPixmap(width,height);
      module->fill(Qt::gray);
      return QIcon(*module);
    }
    return QIcon(m_modules.value(index.row())->imgDir());
  }
  else if (role == Qt::UserRole)
    return m_modules.value(index.row())->name();

  return QVariant();
}

Qt::ItemFlags ModuleModel::flags(const QModelIndex &index) const
{
  if (index.isValid())
      return (QAbstractItemModel::flags(index)|Qt::ItemIsDragEnabled);

  return Qt::ItemIsDragEnabled;
}

QMimeData *ModuleModel::mimeData(const QModelIndexList &indexes) const
{
  QMimeData *mimeData = new QMimeData();
  QByteArray encodedData;

  QDataStream stream(&encodedData, QIODevice::WriteOnly);

  foreach (QModelIndex index, indexes) {
      if (index.isValid()) {
          QIcon icon=qvariant_cast<QIcon>(data(index,Qt::DecorationRole));
          QString gateName = qvariant_cast<QString>(data(index,Qt::UserRole));
          stream << icon << gateName;
          qDebug()<<"Gate name on model"<<gateName;
      }
  }

  mimeData->setData("module->x", encodedData);
  qDebug()<<"Mime Formats "<< mimeData->formats();
  return mimeData;

}

QStringList ModuleModel::mimeTypes() const
{
  QStringList types;
  types << "module->x";
  return types;
}

int ModuleModel::addGate(Gate *gate)
{
  int row = m_gates.size();

  beginInsertRows(QModelIndex(), row, row);
  m_gates.insert(row, gate);
  endInsertRows();

  return 0;
}

int ModuleModel::addModule(Module *module)
{
  int row = m_modules.size();

  beginInsertRows(QModelIndex(), row, row);
  m_modules.insert(row, module);
  endInsertRows();

  return 0;
}

int ModuleModel::addModule(QList<Module *> &modules)
{
  for(auto it : modules)
  {
    addModule(it);
  }
  return 0;
}





