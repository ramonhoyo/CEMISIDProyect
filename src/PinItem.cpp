#include "PinItem.h"
#include <QPen>
#include <QBrush>
#include <GraphicsModuleItem.h>

#include <QDebug>


PinItem::PinItem(QGraphicsItem *parent):
  QGraphicsEllipseItem(parent),
  separation(0),
  type(PINTYPE::INPUT)
{
  // Empty //
}

PinItem::PinItem(qreal _separation, PINTYPE _type, QGraphicsItem *parent):
  QGraphicsEllipseItem(parent),
  separation(_separation),
  type(_type)

{
  QPen * pen = new QPen(QBrush(),6);

  if(type == PINTYPE::INPUT)
    pen->setColor(Qt::blue);
  else{
    pen->setColor(Qt::red);
    if(this->parentItem() != 0)
    {
      GraphicsModuleItem * parent= static_cast<GraphicsModuleItem*>(this->parentItem());
      if(parent != 0)
      {
        this->setPos(parent->pixmap().width(),0);
      }
    }
  }
  setPen(*pen);
  setFlag(ItemIsSelectable );
  setRect(0,separation,7,7);

}

PinItem::PinItem(const QRectF &rect, PINTYPE _type, QGraphicsItem *parent):
  QGraphicsEllipseItem(rect,parent),
  type(_type)

{
  QPen * pen = new QPen(QBrush(),4);
  if(type == PINTYPE::INPUT)
    pen->setColor(Qt::blue);
  else{
    pen->setColor(Qt::red);
    if(this->parent() != 0)
    {
      GraphicsModuleItem * parent= static_cast<GraphicsModuleItem*>(this->parent());
      if(parent != 0)
        this->setPos(parent->pixmap().width(),0);
    }
  }  this->setPen(*pen);
  this->setFlag(ItemIsSelectable);
}


PinItem::PinItem(qreal x1, qreal y1, qreal width, qreal height, qreal _separation,
                 PINTYPE _type, QGraphicsItem * parent):
  QGraphicsEllipseItem(parent),
  separation(_separation),
  type(_type)
{
  QPen * pen = new QPen(QBrush(),5);
  if(type == PINTYPE::INPUT)
    pen->setColor(Qt::blue);
  else{
    pen->setColor(Qt::red);
    if(this->parent() != 0)
    {
      GraphicsModuleItem * parent= static_cast<GraphicsModuleItem*>(this->parent());
      if(parent != 0)
        this->setPos(parent->pixmap().width(),0);
    }
  }  this->setPen(*pen);
  this->setFlag(ItemIsSelectable);
  this->setRect(x1,y1+_separation,width,height);
}

