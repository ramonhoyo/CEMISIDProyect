#include "GraphicsView.h"
#include <QMimeData>
#include <QDropEvent>
#include <GraphicsModuleItem.h>
#include <QDebug>
#include <QIcon>
#include <PinItem.h>

GraphicsView::GraphicsView(QWidget *parent):
  QGraphicsView(parent)
{
  setAcceptDrops(true);
  m_moduleSelect = false;
  m_pinSelectect = false;
}
GraphicsView::GraphicsView(QGraphicsScene *scene ,QWidget *parent):
  QGraphicsView(scene,parent)
{
  scene = nullptr;
  setAcceptDrops(true);
  m_moduleSelect = false;
  m_pinSelectect = false;
}

GraphicsView::~GraphicsView()
{
  qDeleteAll(m_itemsCreated.begin(), m_itemsCreated.end());
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
  GraphicsModuleItem * moduleFromCursor = dynamic_cast<GraphicsModuleItem*>
      (this->itemAt(event->pos()));

  if(moduleFromCursor != 0 and m_moduleSelect) //! if some Module is moving then.
  {
    QPointF point = (QPointF)event->pos();
    moduleFromCursor->setZValue(1);
    moduleFromCursor->setPositionGate(point);
    event->accept();
    QList<QGraphicsItem*> childItems=moduleFromCursor->childItems();

    //! Adjust lines connections items !
    for( auto it : childItems)
    {
      PinItem * pin = dynamic_cast<PinItem*>(it);
      if(pin != 0)
        emit(pin->positionChanged( QPoint(pin->scenePos().x(),
                                          pin->scenePos().y()+pin->getSepartation())));
    }
  }
  else
    event->ignore();
  QGraphicsView::mouseMoveEvent(event);
}

void GraphicsView::mouseDoubleClickEvent(QMouseEvent *event)
{

  PinItem * pinFromCursor = dynamic_cast<PinItem*>(this->itemAt(event->pos()));

  GraphicsModuleItem * moduleFromCursor = dynamic_cast<GraphicsModuleItem*>
      (this->itemAt(event->pos()));

  qDebug()<<"mouse doubleClick event";

  if(pinFromCursor != 0) // is selected one pin.
  {

    qDebug()<<"detected pin selected in pos "<<event->pos();
    m_intialPointConection = event->pos();
    m_pinSelectect = true;
    event->accept();
  }
  else if(moduleFromCursor != 0)
  {
    qDebug()<<"found item" << moduleFromCursor->nameModule();
    m_moduleSelect = true;
    event->accept();
  }
  else
  {
    qDebug()<<"not found item or pin";
    m_moduleSelect = false;
    m_pinSelectect = false;
    event->ignore();
  }
  QGraphicsView::mouseDoubleClickEvent(event);
}

void GraphicsView::mouseReleaseEvent(QMouseEvent *event)
{

  m_moduleSelect = false;
  GraphicsModuleItem * moduleFromCursor = dynamic_cast<GraphicsModuleItem*>
      (this->itemAt(event->pos()));

  PinItem * pinFromCursor = dynamic_cast<PinItem*>(this->itemAt(event->pos()));

  if(pinFromCursor != 0 and m_pinSelectect ) // if mouse is release on one pin item
  {
    //obtain pin sender.
    PinItem * pinSender = dynamic_cast<PinItem*>(this->itemAt(m_intialPointConection));

    //! obtain module sender.
    GraphicsModuleItem * moduleInitialPoint = dynamic_cast<GraphicsModuleItem*>
        (pinSender->parentItem());
    //! obtain module recepter.
    GraphicsModuleItem * moduleEndPoint = dynamic_cast<GraphicsModuleItem*>
        (pinFromCursor->parentItem());

    // if is found module sender and recepter then.
    if(moduleEndPoint != 0 and moduleInitialPoint != 0)
    {
      if(moduleEndPoint != moduleInitialPoint ) // if module sender and recepter are distinct
      {
        //! create connection !

        //! map pos to scene of point start and point end , and crearte connection!
        WireConnection * wire = new WireConnection(mapToScene( m_intialPointConection),
                                                   mapToScene((event->pos())));
        //! ajust wire connection when pin sender changed position.
        connect(pinSender,SIGNAL(positionChanged(QPointF)),wire,SLOT(ajustStart(QPointF)));
        //! ajust wire connection when pin recerpter changed position.
        connect(pinFromCursor,SIGNAL(positionChanged(QPointF)),wire,SLOT(ajustEnd(QPointF)));

        //! add wire to scene. and append to pins created list.
        this->scene()->addItem(wire);
        m_pinsCreated.append(wire);
      }
    }
    m_intialPointConection = QPoint(); // other case point start will be a empty point.(restart)
  }

  if(moduleFromCursor != 0)
    moduleFromCursor->setZValue(-1);

  m_pinSelectect = false;

  QGraphicsView::mouseReleaseEvent(event);
}

void GraphicsView::mousePressEvent(QMouseEvent *event)
{
  event->ignore();
}

int GraphicsView::setModuleDataBase(QHash<QString, Module *> *moduleDataBase)
{
  if(moduleDataBase) // if _gatesDataBase isn't empty.
  {
   m_modulesDataBase = moduleDataBase;
   return 0;
  }
  return 1;
}

int GraphicsView::addModuleToScene(Module * module)
{
  GraphicsModuleItem * moduleItem = new GraphicsModuleItem(0,module);
  m_itemsCreated.append(moduleItem);

  this->scene()->addItem(moduleItem);

  return 0;
}

void GraphicsView::zoomIn()
{
  this->scale(1.2,1.2);
}

void GraphicsView::zoomOut()
{
  this->scale(0.8,0.8);
}


void GraphicsView::dropEvent(QDropEvent *event)
{

  if (event->mimeData()->hasFormat("module->x") )
  {
    QByteArray dataEncoded = event->mimeData()->data("module->x");

    QDataStream stream(&dataEncoded,QIODevice::ReadOnly);
    while (!stream.atEnd())
    {
      QString nameModule;
      QIcon icon;
      stream >> icon >> nameModule;

      Module *moduleFromMimeTipe = new Module( * m_modulesDataBase->value(nameModule));
      if(moduleFromMimeTipe == 0)
        return;

      moduleFromMimeTipe->setPosition(mapToScene( event->pos()));
      emit(moduleAddToProject(moduleFromMimeTipe));
      addModuleToScene(moduleFromMimeTipe);
    }
  }
  else
    event->ignore();

}

void GraphicsView::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasFormat("module->x"))
  {
    event->setDropAction(Qt::MoveAction);
    event->accept();
  }
  else
    event->ignore();
}

void GraphicsView::dragMoveEvent(QDragMoveEvent *event)
{
  if (event->mimeData()->hasFormat("module->x"))
  {
    event->setDropAction(Qt::MoveAction);
    event->accept();
  }
  else
    event->ignore();
}
